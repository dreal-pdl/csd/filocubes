# Crédits - Mentions légales

Publié le 09 juin 2022

## Service gestionnaire

Direction Régionale de l’Environnement de l’Aménagement et du Logement des Pays de la Loire

5, rue Françoise Giroud

CS 16326

44263 NANTES Cedex 2

Tél : 02 72 74 73 00

Courriel : statistiques.dreal-pdl [at] developpement-durable.gouv.fr

## Directrice de publication

Anne Beauval, directrice régionale de l’environnement, de l’aménagement et du logement des Pays de la Loire.

## Conception, Réalisation

- Charte graphique : mise en conformité avec la charte graphique de l'État : <a href="https://github.com/spyrales/shinygouv" target="_blank">Shinygouv</a>

- Conception et développement : DREAL Pays de la Loire - Daniel Kalioudjoglou

## Code source

L’ensemble des scripts de collecte et de datavisualisation est disponible sur <a href="https://gitlab-forge.din.developpement-durable.gouv.fr/dreal-pdl/csd/filocubes" target="_blank">répertoire gitlab du DREAL datalab</a>. 

## Hébergement

- serveur Rstudio - DREAL Pays de la Loire

## Droit d’auteur
Tous les contenus présents sur le site de la direction régionale de l’Environnement, de l’Aménagement et du Logement des Pays de la Loire sont couverts par le droit d’auteur. Toute reprise est dès lors conditionnée à l’accord de l’auteur en vertu de l’article L.122-4 du Code de la Propriété Intellectuelle.

## Usage

- Les utilisateurs sont responsables des interrogations qu’ils formulent ainsi que de l’interprétation et de l’utilisation qu’ils font des résultats. Il leur appartient d’en faire un usage conforme aux réglementations en vigueur et aux recommandations de la CNIL lorsque des données ont un caractère nominatif (loi n° 78.17 du 6 janvier 1978, relative à l’informatique, aux fichiers et aux libertés dite loi informatique et libertés). 

- Il appartient à l’utilisateur de ce site de prendre toutes les mesures appropriées de façon à protéger ses propres données et/ou logiciels de la contamination par d’éventuels virus circulant sur le réseau Internet. De manière générale, la Direction Régionale de l’Environnement de l’Aménagement et du Logement des Pays de la Loire décline toute responsabilité à un éventuel dommage survenu pendant la consultation du présent site. Les messages que vous pouvez nous adresser transitant par un réseau ouvert de télécommunications, nous ne pouvons assurer leur confidentialité.
