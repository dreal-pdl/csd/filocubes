**Filocubes** est une application de diffusion des données statistiques issues de Filocom

Code source de l'application : <https://gitlab-forge.din.developpement-durable.gouv.fr/dreal-pdl/csd/filocubes>

Lien vers l'application (intranet) : <http://set-pdl-dataviz.dreal-pdl.ad.e2.rie.gouv.fr/filocubes/>

L'objectif de Filocubes, est de **faciliter la mise à disposition des cubes** (tableaux agrégés et secrétisés) issus de Filocom (Fichier des logements à l'échelle communale). Ces cubes sont produits par le SDES.  

L'application simplifie au maximum **l'extraction des données selon le zonage et le millésime souhaités**. Le résultat de la sélection est alors **importé dans un fichier unique**.

<div class = "row">
<div class = "col-md-8">
<img style="float: left;margin:5px 20px 5px 0px" src="www/imp_ecran2.jpg" width="100%">
</div>
<div class = "col-md-4">
<br>

</div>
</div>
