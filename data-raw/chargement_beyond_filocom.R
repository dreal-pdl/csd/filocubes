# Création des tables nationales

library(dplyr)
library(stringr)
library(purrr)
library(COGiter)

# fichiers à intégrer
num_fichiers <- c("01","02","03","04a","04b","05","06","07","08","09a","09b","10",
                  "11","12","13","14","15a","15b","16","17a","17b",
                  "fmonop_04a","fmonop_09a","fmonop_11","fmonop_13","fmonop_17a"
                  )
noms_fichiers <- c("Le parc de logements","Les propri\u00e9taires des logements","Les r\u00e9sidences principales, par statuts d\'occupation",
                   "Caract\u00e9ristiques des m\u00e9nages fiscaux par statuts d\'occupation","Caract\u00e9ristiques des m\u00e9nages fiscaux par statuts d\'occupation",
                   "Les propri\u00e9taires des r\u00e9sidences principales priv\u00e9es","La vacance dans le parc priv\u00e9 et dans le parc public",
                   "Construction: le parc des logements construits (hors restructuration) au cours des 10 derni\u00e8res ann\u00e9es",
                   "Les r\u00e9sidences principales construites (hors restructuration) au cours des 10 derni\u00e8res ann\u00e9es, par statuts d\'occupation",
                   "Les occupants des logements construits (hors restructuration) au cours des 10 derni\u00e8res ann\u00e9es, par statuts d\'occupation",
                   "Les occupants des logements construits (hors restructuration) au cours des 10 derni\u00e8res ann\u00e9es, par statuts d\'occupation",
                   "Suivi du parc sur 2 ann\u00e9es cons\u00e9cutives","Les conditions d\u2019habitat des propri\u00e9taires occupants \u00e9ligibles aux aides de l\u2019Anah",
                   "Le parc locatif priv\u00e9 de plus de 15 ans","Les occupants du parc locatif priv\u00e9 de plus de 15 ans",
                   "Le parc des logements en copropri\u00e9t\u00e9","Les r\u00e9sidences principales en copropri\u00e9t\u00e9",
                   "Les r\u00e9sidences principales en copropri\u00e9t\u00e9","Les propri\u00e9taires des logements en copropri\u00e9t\u00e9",
                   "Les caract\u00e9ristiques des m\u00e9nages fiscaux vivant dans des logements en copropri\u00e9t\u00e9 par statuts d\'occupation",
                   "Les caract\u00e9ristiques des m\u00e9nages fiscaux vivant dans des logements en copropri\u00e9t\u00e9 par statuts d\'occupation",
                   #familles monoparentales
                   "familles monoparentales (ancienne série) - Caract\u00e9ristiques des m\u00e9nages fiscaux par statuts d\'occupation",
                   "familles monoparentales (ancienne série) - Les occupants des logements construits (hors restructuration) au cours des 10 derni\u00e8res ann\u00e9es, par statuts d\'occupation",
                   "familles monoparentales (ancienne série) - Les conditions d\u2019habitat des propri\u00e9taires occupants \u00e9ligibles aux aides de l\u2019Anah",
                   "familles monoparentales (ancienne série) - Les occupants du parc locatif priv\u00e9 de plus de 15 ans",
                   "familles monoparentales (ancienne série) - Les caract\u00e9ristiques des m\u00e9nages fiscaux vivant dans des logements en copropri\u00e9t\u00e9 par statuts d\'occupation"
                   )

tableau_liste_num <- data.frame(x = num_fichiers, y = noms_fichiers) %>%
  mutate(x = as.character(x), y = as.character(y))

# liste des zones France entiere a partir de COGiter
liste_des_zones <- liste_zone  %>%
  mutate(TypeZone = str_replace(TypeZone,"Communes","COM"),
         TypeZone = str_replace(TypeZone,"Epci","EPCI"),
         TypeZone = str_replace(TypeZone,"D\u00e9partements","DEP"),
         TypeZone = str_replace(TypeZone,"R\u00e9gions","REG"))  %>%
  mutate(code_lien = paste0(TypeZone, CodeZone))  %>%
  select(code_lien, EPCI, DEP, REG)

# creation de la table des arrondissements de Marseille, Lyon et Paris
arrond <- tibble::tribble(
  ~code_lien, ~EPCI, ~DEP, ~REG,
  "COM13201","200054807","13","93", # Marseille 1er Arrondissement
  "COM13202","200054807","13","93",
  "COM13203","200054807","13","93",
  "COM13204","200054807","13","93",
  "COM13205","200054807","13","93",
  "COM13206","200054807","13","93",
  "COM13207","200054807","13","93", # Marseille 7e Arrondissement
  "COM13208","200054807","13","93",
  "COM13209","200054807","13","93",
  "COM13210","200054807","13","93",
  "COM13211","200054807","13","93",
  "COM13212","200054807","13","93",
  "COM13213","200054807","13","93",
  "COM13214","200054807","13","93",
  "COM13215","200054807","13","93",
  "COM13216","200054807","13","93", #"Marseille 16e Arrondissement"
  "COM69381","200046977","69","84", #Lyon 1er Arrondissement
  "COM69382","200046977","69","84",
  "COM69383","200046977","69","84",
  "COM69384","200046977","69","84",
  "COM69385","200046977","69","84",
  "COM69386","200046977","69","84",
  "COM69387","200046977","69","84",
  "COM69388","200046977","69","84",
  "COM69389","200046977","69","84", #Lyon 9e Arrondissement"
  "COM75101","200054781","75","11", # Paris 1er Arrondissement
  "COM75102","200054781","75","11",
  "COM75103","200054781","75","11",
  "COM75104","200054781","75","11",
  "COM75105","200054781","75","11",
  "COM75106","200054781","75","11",
  "COM75107","200054781","75","11",
  "COM75108","200054781","75","11",
  "COM75109","200054781","75","11",
  "COM75110","200054781","75","11",
  "COM75111","200054781","75","11",
  "COM75112","200054781","75","11",
  "COM75113","200054781","75","11",
  "COM75114","200054781","75","11",
  "COM75115","200054781","75","11",
  "COM75116","200054781","75","11",
  "COM75117","200054781","75","11",
  "COM75118","200054781","75","11",
  "COM75119","200054781","75","11",
  "COM75120","200054781","75","11", #Paris 20e Arrondissement"
) %>%
  mutate_if(is.character, as.factor)

liste_des_zones <- rbind(liste_des_zones,arrond)

# Fonction de création des tables
creer_tab <- function (x){
  num <-x
  # ouvre les fichiers csv differement suivant le format
  cube <- read.csv2(paste0("extdata/Beyond/", x,".csv"), header = TRUE, sep=";", stringsAsFactors = TRUE, fileEncoding  = "utf-8", check.names = FALSE) %>%
      # rename ("zone_geo"= X , "annee" = libelle) %>%
    rename ("zone_geo"= 1 ) %>%
    filter (annee != "annee")
  
  # scinde la colonne zonage en 3 colonnes
  cube %>%
    mutate("zone_geo"= as.character(zone_geo)) %>%
    mutate(TypeZone = case_when(
      grepl("- REG", zone_geo, perl = TRUE) ~ "REG",
      grepl("- DEP", zone_geo, perl = TRUE) ~ "DEP",
      grepl("- COM", zone_geo, perl = TRUE) ~ "COM",
      grepl("- UU", zone_geo, perl = TRUE) ~ "UU",
      grepl("- EPCI", zone_geo, perl = TRUE) ~ "EPCI",
      grepl("- ZE", zone_geo, perl = TRUE) ~ "ZE",
      grepl("FRANCE MÉTROPOLITAINE", zone_geo, perl = TRUE) ~ "NATIONAL",
      grepl("RÉGIONS D'OUTRE-MER", zone_geo, perl = TRUE) ~ "NATIONAL",
      TRUE  ~ zone_geo
    )) %>%
    mutate(CodeZone = case_when(
      TypeZone == "REG" ~ substr(zone_geo,1,2),
      TypeZone == "DEP"~ substr(zone_geo,1,3),
      TypeZone == "COM" ~ substr(zone_geo,1,5),
      TypeZone == "UU" ~ substr(zone_geo,1,5),
      TypeZone == "EPCI" ~ substr(zone_geo,1,9),
      TypeZone == "ZE" ~ substr(zone_geo,1,4),
      grepl("FRANCE MÉTROPOLITAINE", zone_geo, perl = TRUE) ~ "N1",
      grepl("RÉGIONS D'OUTRE-MER", zone_geo, perl = TRUE) ~ "N2",
      TRUE  ~ "SUPPRIMER"
    )) %>%
    mutate(CodeZone=str_replace_all(CodeZone," ",""))%>%
    mutate(Zone = case_when(
      TypeZone == "REG" ~ substr(zone_geo, 6, nchar(zone_geo)-6),
      TypeZone == "DEP"~ substr(zone_geo, nchar(CodeZone)+4, nchar(zone_geo)-6),
      TypeZone == "COM" ~ substr(zone_geo, 9, nchar(zone_geo)-6),
      TypeZone == "UU" ~ substr(zone_geo, 9, nchar(zone_geo)-5),
      TypeZone == "EPCI" ~ substr(zone_geo, 13, nchar(zone_geo)-7),
      TypeZone == "ZE" ~ substr(zone_geo, 8, nchar(zone_geo)-5),
      TypeZone == "NATIONAL" ~ zone_geo,
      TRUE  ~ "SUPPRIMER"
    )) %>%
    filter(TypeZone %in% c("REG","DEP","COM","EPCI","FRANCE M\u00c9TROPOLITAINE","R\u00c9GIONS D\'OUTRE-MER","ZE","UU","NATIONAL")) %>%
    select(- zone_geo) %>%
    relocate (CodeZone,Zone,TypeZone)%>%
    # ajout des colonnes d'appartenance aux EPCI,départements et régions
    mutate(code_lien = paste0(TypeZone,CodeZone))  %>%
    left_join(liste_des_zones, by = c("code_lien"))  %>%
    select(-code_lien)  %>%
    relocate (EPCI,DEP,REG,.after = TypeZone) %>%
    mutate(EPCI = as.character(EPCI),DEP= as.character(DEP),"REG" = as.character(REG) )
}

# Lancement de la fonction
# tous les df dans une liste
df_liste <- map(.x = num_fichiers, .f = ~ creer_tab(.x)) %>%
  # on nomme les df de la liste
  set_names(paste0("cube_", num_fichiers))

# création liste des régions
# liste_reg <- as.character(unique(regions$NOM_REG))
liste_reg <- regions %>%
  pull(REG) %>%
  as.character() %>%
  setNames(regions %>% pull(NOM_REG) %>% as.character())

# adaptation du cube 10 (année)
df_liste$cube_10 <- df_liste$cube_10  %>%
  rename(periode = annee) %>%
  mutate(periode = sprintf("%04d",periode)) %>%
  mutate(periode = paste0("20",substr(periode, 1, 2),"-20",substr(periode, 3, 4))) %>% 
  mutate(annee = substr(periode, 6, 9),
          annee = as.integer(annee)) %>%
  relocate(annee, .before = periode)

df_liste <- df_liste %>%
  setNames(c("01","02", "03","04a","04b","05","06","07","08","09a","09b","10","11","12","13","14","15a","15b","16","17a","17b",
             "fmonop_04a","fmonop_09a","fmonop_11","fmonop_13","fmonop_17a"))

rm(noms_fichiers, creer_tab,liste_des_zones,arrond)
save(list = ls(), file="Rdata/beyond_filocom.RData")
